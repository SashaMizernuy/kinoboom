package com.example.kinoboom.dialogSheet

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.kinoboom.R
import com.example.kinoboom.listFilm.FilmListFragment.Companion.SORT_BUNDLE_KEY
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

import kotlinx.android.synthetic.main.fragment_sheet_dialog.view.*


class SortDialogFragment : BottomSheetDialogFragment() {

    companion object SortDialogEnum {
        const val DATE_VAL = "date"
        const val RATE_VAL = "rate"
    }

    var sortSheetListener: SortSheetListener? = null

    fun setSelectedListener(sortSheetListener: SortSheetListener) {
        this.sortSheetListener = sortSheetListener
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_sheet_dialog, container, false)
        val sortArg = arguments?.getString(SORT_BUNDLE_KEY)
        sortArg?.let { setColorText(view, it) }
        view.date.setOnClickListener {
            sortSheetListener?.onDateSortClicked()
            dismiss()
        }
        view.rating.setOnClickListener {
            sortSheetListener?.onRatingSortClicked()
            dismiss()
        }
        return view
    }

    interface SortSheetListener {
        fun onDateSortClicked()

        fun onRatingSortClicked()
    }

    fun setColorText(view: View, sortArg: String) {
        when (sortArg) {
            DATE_VAL -> view.date.setTextColor(Color.RED)
            RATE_VAL -> view.rating.setTextColor(Color.RED)
        }
    }
}