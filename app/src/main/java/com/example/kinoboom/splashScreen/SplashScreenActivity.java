package com.example.kinoboom.splashScreen;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.example.kinoboom.R;
import com.example.kinoboom.auth.AuthActivity;
import com.example.kinoboom.token.FacebookRequest;
import com.example.kinoboom.token.GoogleRequest;
import com.example.kinoboom.view.MainActivity;


public class SplashScreenActivity extends AppCompatActivity implements SplashContract.View {

    private final int SPLASH_TIME = 1000;
    private SplashPresenter presenter;
    private GoogleRequest googleRequest;
    private FacebookRequest facebookRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_layout);
        googleRequest = new GoogleRequest(this);
        facebookRequest = new FacebookRequest();
        initPresenter();
    }

    public void initPresenter() {
        presenter = new SplashPresenter(this, new SplashInteractor(googleRequest, facebookRequest));
        presenter.onViewCreated();
    }

    @Override
    public void startSplashScreen() {
        new Handler().postDelayed(() -> presenter.signInRequestStarted(), SPLASH_TIME);
    }

    @Override
    public void showAuthorizationFragment() {
        Intent intent = new Intent(this, AuthActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void showMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
