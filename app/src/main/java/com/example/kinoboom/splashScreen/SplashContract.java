package com.example.kinoboom.splashScreen;


public interface SplashContract {

    interface View {
        void startSplashScreen();

        void showAuthorizationFragment();

        void showMainActivity();
    }

    interface Presenter {
        void onViewCreated();

        void signInRequestStarted();
    }
}
