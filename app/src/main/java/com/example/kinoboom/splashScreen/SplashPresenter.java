package com.example.kinoboom.splashScreen;


public class SplashPresenter implements SplashInteractor.OnSignInListener, SplashContract.Presenter {

    private final SplashContract.View view;
    private SplashInteractor splashInteractor;


    public SplashPresenter(SplashContract.View view, SplashInteractor splashInteractor) {
        this.view = view;
        this.splashInteractor = splashInteractor;
    }

    @Override
    public void onViewCreated() {
        view.startSplashScreen();
    }

    @Override
    public void signInRequestStarted() {
        splashInteractor.checkSignIn(this);
    }

    @Override
    public void authSuccessful() {
        view.showMainActivity();
    }

    @Override
    public void authFailed() {
        view.showAuthorizationFragment();
    }
}
