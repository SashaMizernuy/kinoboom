package com.example.kinoboom.splashScreen

import com.example.kinoboom.token.FacebookRequest
import com.example.kinoboom.token.GoogleRequest


class SplashInteractor(val googleRequest: GoogleRequest, val facebookRequest: FacebookRequest) {

    fun checkSignIn(listener: OnSignInListener) {
        when (googleRequest.isAuth() ?: false || facebookRequest.isAuth() ?: false) {
            true -> listener.authSuccessful()
            else -> listener.authFailed()
        }
    }

    interface OnSignInListener {
        fun authSuccessful()

        fun authFailed()
    }
}