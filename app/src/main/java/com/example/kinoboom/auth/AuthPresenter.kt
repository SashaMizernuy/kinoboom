package com.example.kinoboom.auth

class AuthPresenter(private var view: AuthContract.View) : AuthContract.Presenter {

    override fun onViewCreated() {
        view.startAuthorizationFragment()
    }
}