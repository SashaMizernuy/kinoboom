package com.example.kinoboom.auth

interface AuthContract {

    interface View {
        fun startAuthorizationFragment()
    }

    interface Presenter {
        fun onViewCreated()
    }
}