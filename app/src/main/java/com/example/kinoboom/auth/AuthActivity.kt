package com.example.kinoboom.auth

import android.os.Bundle

import androidx.appcompat.app.AppCompatActivity

import com.example.kinoboom.R
import com.example.kinoboom.authorization.AuthorizationFragment


class AuthActivity : AppCompatActivity(), AuthContract.View {

    lateinit var presenter: AuthPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
        initPresenter()
    }

    fun initPresenter() {
        presenter = AuthPresenter(this)
        presenter.onViewCreated()
    }

    override fun startAuthorizationFragment() {
        supportFragmentManager
                .beginTransaction()
                .add(R.id.signInFragment, AuthorizationFragment())
                .commit()
    }
}
