package com.example.kinoboom.sharedPreferences

import android.content.Context
import android.content.SharedPreferences


class SharedPreferencesUtil(val context: Context?) {

    companion object SharedKeys {
        val PREFS_SORT_RATE_VAL = "rate"
        val PREFS_SORT_DATE_VAL = "date"
        val PREFS_NAME = "saving_values"
    }

    val sharedPreferences: SharedPreferences? = context?.getSharedPreferences("Data", Context.MODE_PRIVATE)
    val editor: SharedPreferences.Editor? = sharedPreferences?.edit()

    fun putSortRate() {
        editor?.putString(PREFS_NAME, PREFS_SORT_RATE_VAL)
        editor?.commit()
    }

    fun putSortDate() {
        editor?.putString(PREFS_NAME, PREFS_SORT_DATE_VAL)
        editor?.commit()
    }

    fun getFilmSortType(): String? {
        return sharedPreferences?.getString(PREFS_NAME, "").toString()
    }
}