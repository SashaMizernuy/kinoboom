package com.example.kinoboom.authorization

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.fragment.app.Fragment

import com.example.kinoboom.R
import com.example.kinoboom.token.FacebookRequest
import com.example.kinoboom.token.GoogleRequest
import com.example.kinoboom.view.MainActivity
import com.facebook.CallbackManager
import com.facebook.login.LoginManager

import kotlinx.android.synthetic.main.fragment_authorization.view.*


class AuthorizationFragment : Fragment(), AuthorizationContract.View {

    lateinit var presenter: AuthorizationPresenter
    lateinit var googleRequest: GoogleRequest
    lateinit var facebookRequest: FacebookRequest
    private var callbackManager: CallbackManager? = null

    companion object {
        const val REQUEST_CODE = 9001
        const val READ_PERMISSION = "email"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_authorization, container, false)
        googleRequest = GoogleRequest(this.context)
        facebookRequest = FacebookRequest()
        initPresenter()
        view.googleButton.setOnClickListener { presenter.onGoogleButtonClicked() }
        view.facebookButton.setOnClickListener { presenter.onFaceBookButtonClicked() }
        return view
    }

    fun initPresenter() {
        presenter = AuthorizationPresenter(this)
    }

    override fun signInGoogle() {
        startActivityForResult(googleRequest.signInGoogle(), REQUEST_CODE)
    }

    override fun signInFacebook() {
        callbackManager = facebookRequest.getCallbackManager()
        LoginManager.getInstance().logInWithReadPermissions(this, listOf(READ_PERMISSION))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager?.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == -1) {
            presenter.loginIsCompleted()
        }
    }

    override fun startMainActivity() {
        val intent = Intent(activity, MainActivity::class.java)
        activity?.startActivity(intent)
        activity?.finish()
    }
}
