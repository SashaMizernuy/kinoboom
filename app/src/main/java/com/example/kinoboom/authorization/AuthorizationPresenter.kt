package com.example.kinoboom.authorization


class AuthorizationPresenter(private var view: AuthorizationContract.View) : AuthorizationContract.Presenter {

    override fun onGoogleButtonClicked() {
        view.signInGoogle()
    }

    override fun onFaceBookButtonClicked() {
        view.signInFacebook()
    }

    override fun loginIsCompleted() {
        view.startMainActivity()
    }
}

