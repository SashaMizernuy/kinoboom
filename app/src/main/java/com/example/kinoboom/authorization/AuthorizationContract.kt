package com.example.kinoboom.authorization


interface AuthorizationContract {

    interface View {
        fun signInGoogle()

        fun signInFacebook()

        fun startMainActivity()
    }

    interface Presenter {
        fun onGoogleButtonClicked()

        fun onFaceBookButtonClicked()

        fun loginIsCompleted()
    }
}