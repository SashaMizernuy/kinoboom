package com.example.kinoboom.view;


public interface MainContract {

    interface View {
        void showFilmListFragment();

        void showMenu();
    }

    interface Presenter {
        void onViewCreated();
    }
}
