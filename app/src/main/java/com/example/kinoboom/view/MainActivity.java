package com.example.kinoboom.view;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.kinoboom.R;
import com.example.kinoboom.listFilm.FilmListFragment;
import com.example.kinoboom.menu.MenuFragment;


public class MainActivity extends AppCompatActivity implements MainContract.View {

    private MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initMainPresenter();
    }

    public void initMainPresenter() {
        presenter = new MainPresenter(this);
        presenter.onViewCreated();
    }

    @Override
    public void showFilmListFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.containerFragment, new FilmListFragment(), "FilmListFragment")
                .commit();
    }

    @Override
    public void showMenu() {
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.menuFragment, new MenuFragment())
                .commit();
    }
}