package com.example.kinoboom.token

import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.login.LoginManager


class FacebookRequest {

    fun isAuth(): Boolean? {
        val accessToken = AccessToken.getCurrentAccessToken()
        return !(accessToken?.isExpired ?: true)
    }

    fun getCallbackManager(): CallbackManager {
        return CallbackManager.Factory.create()
    }

    fun logOutFacebook() {
        LoginManager.getInstance().logOut()
    }
}