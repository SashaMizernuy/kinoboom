package com.example.kinoboom.token

import android.content.Context
import android.content.Intent

import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions


class GoogleRequest(val context: Context?) {

    fun isAuth(): Boolean? {
        val googleAccount = GoogleSignIn.getLastSignedInAccount(context)
        return !(googleAccount?.isExpired ?: true)
    }

    fun logOutGoogle() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()
        val mGoogleSignInClient = context?.let { GoogleSignIn.getClient(it, gso) }
        mGoogleSignInClient?.signOut()
    }

    fun signInGoogle(): Intent? {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()
        val mGoogleSignInClient = context?.let { GoogleSignIn.getClient(it, gso) }
        val signInIntent = mGoogleSignInClient?.getSignInIntent()
        return signInIntent
    }
}