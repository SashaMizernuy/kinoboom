package com.example.kinoboom.recyclerAdapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kinoboom.databinding.FilmCardLayoutBinding;
import com.example.kinoboom.modal.Film;

import java.util.List;


public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.FilmAdapterViewHolder> {

    public OnItemClickListener clickListener;
    public OnItemLongClickListener longClickListener;
    public List<Film> filmList;

    public RecyclerAdapter(List<Film> filmList,
                           OnItemClickListener clickListener,
                           OnItemLongClickListener longClickListener) {
        this.clickListener = clickListener;
        this.longClickListener = longClickListener;
        this.filmList = filmList;
        notifyDataSetChanged();
    }

    @Override
    public FilmAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        FilmCardLayoutBinding binding = FilmCardLayoutBinding.inflate(inflater, parent, false);
        return new FilmAdapterViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(FilmAdapterViewHolder holder, int position) {
        Film film = filmList.get(position);
        holder.binding.setFilm(film);
        holder.binding.setPosition(position);
        holder.binding.setClick(clickListener);
        holder.binding.setLongClick(longClickListener);
    }

    @Override
    public int getItemCount() {
        return filmList.size();
    }

    public interface OnItemClickListener<T> {
        void clicked(T film);
    }

    public interface OnItemLongClickListener<T> {
        boolean longClicked(T film, int position);
    }

    public class FilmAdapterViewHolder extends RecyclerView.ViewHolder {
        FilmCardLayoutBinding binding;

        public FilmAdapterViewHolder(View view) {
            super(view);
            binding = DataBindingUtil.bind(view);
        }
    }
}