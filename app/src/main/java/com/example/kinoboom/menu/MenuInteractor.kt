package com.example.kinoboom.menu

import com.example.kinoboom.token.FacebookRequest
import com.example.kinoboom.token.GoogleRequest


class MenuInteractor(val googleRequest: GoogleRequest, val facebookRequest: FacebookRequest) {

    fun checkSignIn(listener: OnSignInListener) {
        if (googleRequest.isAuth() ?: false) {
            listener.googleSignedIn()
        } else if (facebookRequest.isAuth() ?: false) {
            listener.facebookSignedIn()
        }
    }

    interface OnSignInListener {
        fun googleSignedIn()

        fun facebookSignedIn()
    }
}