package com.example.kinoboom.menu


class MenuPresenter(private var view: MenuContract.View, private var menuInteractor: MenuInteractor)
    : MenuContract.Presenter, MenuInteractor.OnSignInListener {

    override fun onLogOutButtonClicked() {
        menuInteractor.checkSignIn(this)
    }

    override fun onSortButtonClicked() {
        view.initSheetDialog()
    }

    override fun facebookSignedIn() {
        view.logOutFacebook()
    }

    override fun googleSignedIn() {
        view.logOutGoogle()
    }

    override fun logOutCompleted() {
        view.startSplashScreen()
    }
}