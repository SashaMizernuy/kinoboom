package com.example.kinoboom.menu

interface MenuContract {

    interface View {
        fun logOutFacebook()

        fun logOutGoogle()

        fun startSplashScreen()

        fun initSheetDialog()
    }

    interface Presenter {
        fun onLogOutButtonClicked()

        fun onSortButtonClicked()

        fun logOutCompleted()
    }
}