package com.example.kinoboom.menu

import android.content.Intent
import android.os.Bundle
import android.view.*

import androidx.fragment.app.Fragment

import com.example.kinoboom.R
import com.example.kinoboom.listFilm.FilmListFragment
import com.example.kinoboom.splashScreen.SplashScreenActivity
import com.example.kinoboom.token.FacebookRequest
import com.example.kinoboom.token.GoogleRequest


class MenuFragment : Fragment(), MenuContract.View {

    lateinit var presenter: MenuPresenter
    lateinit var googleRequest: GoogleRequest
    lateinit var facebookRequest: FacebookRequest

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_items, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
        googleRequest = GoogleRequest(this.context ?: return)
        facebookRequest = FacebookRequest()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        initPresenter()
        return this.view
    }

    fun initPresenter() {
        presenter = MenuPresenter(this, MenuInteractor(googleRequest, facebookRequest))
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.log_out -> {
                presenter.onLogOutButtonClicked()
                return true
            }
            R.id.sort -> {
                presenter.onSortButtonClicked()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun logOutFacebook() {
        facebookRequest.logOutFacebook()
        presenter.logOutCompleted()
    }

    override fun logOutGoogle() {
        googleRequest.logOutGoogle()
        presenter.logOutCompleted()
    }

    override fun startSplashScreen() {
        val intent = Intent(activity, SplashScreenActivity::class.java)
        activity?.startActivity(intent)
        activity?.finish()
    }

    override fun initSheetDialog() {
        val filmListFragment:FilmListFragment? = fragmentManager?.findFragmentByTag("FilmListFragment") as FilmListFragment?
        filmListFragment?.onMenuSortDialogClicked()
    }
}