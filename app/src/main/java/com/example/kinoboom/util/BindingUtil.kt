package com.example.kinoboom.util

import android.widget.ImageView
import android.widget.RatingBar

import androidx.databinding.BindingAdapter

import com.squareup.picasso.Picasso


class BindingUtil {

    companion object AdapterBinding {
        @JvmStatic
        @BindingAdapter("imageUrl")
        fun loadImage(imageView: ImageView?, url: String?) {
            Picasso.get()?.load("https://image.tmdb.org/t/p/w500/$url")?.fit()?.into(imageView)
        }

        @JvmStatic
        @BindingAdapter("rating")
        fun setRating(ratingBar: RatingBar?, rating: Double?) {
            if (rating != null) {
                ratingBar?.rating = (rating / 100).toFloat()
            }
        }
    }
}