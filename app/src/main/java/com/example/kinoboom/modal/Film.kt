package com.example.kinoboom.modal

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Film(var posterPath: String?, var title: String?,
           var popularity: Double?, var releaseDate: String?,
           var overview: String?) : Parcelable