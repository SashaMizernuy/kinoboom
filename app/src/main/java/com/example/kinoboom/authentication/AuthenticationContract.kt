package com.example.kinoboom.authentication

interface AuthenticationContract {

    interface View {
        fun checkSignIn()

        fun showSignInFragment()

        fun showFilmListFragment()
    }

    interface Presenter {
        fun onViewCreated()

        fun authentificationIsFailed()

        fun authentificationIsSuccessful()
    }
}