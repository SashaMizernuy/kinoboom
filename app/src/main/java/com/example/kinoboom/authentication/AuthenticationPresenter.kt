package com.example.kinoboom.authentication

class AuthenticationPresenter(private var view: AuthenticationContract.View) : AuthenticationContract.Presenter {

    override fun onViewCreated() {
        view.checkSignIn()
    }

    override fun authentificationIsFailed() {
        view.showSignInFragment()
    }

    override fun authentificationIsSuccessful() {
        view.showFilmListFragment()
    }
}