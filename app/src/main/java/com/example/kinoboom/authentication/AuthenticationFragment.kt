package com.example.kinoboom.authentication

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.fragment.app.Fragment

import com.example.kinoboom.R
import com.example.kinoboom.authorization.AuthorizationFragment
import com.example.kinoboom.listFilm.FilmListFragment
import com.google.android.gms.auth.api.signin.GoogleSignIn


class AuthenticationFragment : Fragment(), AuthenticationContract.View {

    lateinit var presenter: AuthenticationPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_authentication, container, false)
        initPresenter()
        return view
    }

    fun initPresenter() {
        presenter = AuthenticationPresenter(this)
        presenter.onViewCreated()
    }

    override fun checkSignIn() {
        val account = GoogleSignIn.getLastSignedInAccount(context)
        when {
            (account == null) ->
                presenter.authentificationIsFailed()
            else ->
                presenter.authentificationIsSuccessful()
        }
    }

    override fun showSignInFragment() {
        fragmentManager
                ?.beginTransaction()
                ?.replace(R.id.containerFragment, AuthorizationFragment())
                ?.commit()
    }

    override fun showFilmListFragment() {
        fragmentManager
                ?.beginTransaction()
                ?.replace(R.id.containerFragment, FilmListFragment(), "listFilmTag")
                ?.commit()
    }
}
