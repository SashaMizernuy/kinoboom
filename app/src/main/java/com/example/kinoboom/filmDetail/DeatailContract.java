package com.example.kinoboom.filmDetail;


public interface DeatailContract {

    interface View {
        void overviewFilm();
    }

    interface Presenter {
        void onViewCreated();
    }
}
