package com.example.kinoboom.listFilm

import com.example.kinoboom.modal.Film
import com.example.kinoboom.viewModal.FilmViewModal


class FilmListPresenter(private val filmViewModal: FilmViewModal,
                        private val filmListInteractor: FilmListInteractor,
                        private val view: FilmListContract.View)
    : FilmListContract.Presenter, FilmListInteractor.OnSortListener {

    override fun onViewCreated() {
        view.progressBarVisible()
        filmViewModal.getCallData(object : FilmViewModal.CallbackInterface {
            override fun accept(responceList: List<Film>) {
                view.addDataResponse(responceList)
                view.initAdapter()
                view.progressBarGone()
            }

            override fun error(text: String) {
                view.showToast(text)
                view.progressBarGone()
            }
        })
    }

    override fun onFilmClicked(film: Film) {
        view.overviewFilm(film)
    }

    override fun onFilmLongClicked(film: Film, position: Int) {
        view.deleteItemDialog(film, position)
    }

    override fun onSortDialogClicked() {
        filmListInteractor.getFilmSortType()?.let { view.showSortDialog(it) }
    }

    override fun onSortByDateButtonClicked() {
        filmListInteractor.putSortDate()
        view.passListFilm()
    }

    override fun onSortByRateButtonClicked() {
        filmListInteractor.putSortRate()
        view.passListFilm()
    }

    override fun listFilmReceived(listFilm: List<Film>) {
        filmListInteractor.sortList(listFilm, this)
        view.notifyAdapterOfChanges()
    }

    override fun requestDataReceived(listFilm: List<Film>) {
        filmListInteractor.sortList(listFilm, this)
    }

    override fun dateSortSelected(sortedListByDate: List<Film>) {
        view.addSortData(sortedListByDate)
    }

    override fun rateSortSelected(sortedListByRate: List<Film>) {
        view.addSortData(sortedListByRate)
    }
}
