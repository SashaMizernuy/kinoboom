package com.example.kinoboom.listFilm

import com.example.kinoboom.modal.Film
import com.example.kinoboom.sharedPreferences.SharedPreferencesUtil
import com.example.kinoboom.sharedPreferences.SharedPreferencesUtil.SharedKeys.PREFS_SORT_DATE_VAL
import com.example.kinoboom.sharedPreferences.SharedPreferencesUtil.SharedKeys.PREFS_SORT_RATE_VAL


class FilmListInteractor(val sharedPreferencesUtil: SharedPreferencesUtil?) {

    fun sortList(listFilm: List<Film>, listener: OnSortListener) {
        when (getFilmSortType()) {
            PREFS_SORT_DATE_VAL -> listener
                    .dateSortSelected(listFilm.sortedWith(compareByDescending { it.releaseDate }))
            PREFS_SORT_RATE_VAL -> listener
                    .rateSortSelected(listFilm.sortedWith(compareByDescending { it.popularity }))
        }
    }

    fun putSortRate() {
        sharedPreferencesUtil?.putSortRate()
    }

    fun putSortDate() {
        sharedPreferencesUtil?.putSortDate()
    }

    fun getFilmSortType(): String? {
        return sharedPreferencesUtil?.getFilmSortType()
    }

    interface OnSortListener {
        fun dateSortSelected(sortedListByDate: List<Film>)

        fun rateSortSelected(sortedListByRate: List<Film>)
    }
}