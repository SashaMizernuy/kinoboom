package com.example.kinoboom.listFilm

import com.example.kinoboom.modal.Film


interface FilmListContract {

    interface View {
        fun progressBarVisible()

        fun addDataResponse(responceList: List<Film>)

        fun showSortDialog(sortType: String)

        fun initAdapter()

        fun progressBarGone()

        fun overviewFilm(film: Film)

        fun deleteItemDialog(film: Film, position: Int)

        fun showToast(text: String)

        fun passListFilm()

        fun notifyAdapterOfChanges()

        fun addSortData(sortedListFilm: List<Film>)
    }

    interface Presenter {
        fun onViewCreated()

        fun onFilmClicked(film: Film)

        fun onFilmLongClicked(film: Film, position: Int)

        fun onSortByDateButtonClicked()

        fun onSortByRateButtonClicked()

        fun listFilmReceived(listFilm: List<Film>)

        fun requestDataReceived(listFilm: List<Film>)

        fun onSortDialogClicked()
    }
}


