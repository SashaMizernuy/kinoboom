package com.example.kinoboom.listFilm

import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast

import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import butterknife.BindView
import butterknife.ButterKnife

import com.example.kinoboom.R
import com.example.kinoboom.app.AppController.getAppComponent
import com.example.kinoboom.dialogSheet.SortDialogFragment
import com.example.kinoboom.filmDetail.DetailFragment
import com.example.kinoboom.modal.Film
import com.example.kinoboom.recyclerAdapter.RecyclerAdapter
import com.example.kinoboom.sharedPreferences.SharedPreferencesUtil
import com.example.kinoboom.viewModal.FilmViewModal

import es.dmoral.toasty.Toasty
import org.parceler.Parcels

import java.util.*
import javax.inject.Inject


class FilmListFragment : Fragment(), FilmListContract.View, SortDialogFragment.SortSheetListener {

    @Inject
    lateinit var filmViewModal: FilmViewModal

    @BindView(R.id.progressBar)
    internal lateinit var progressBar: ProgressBar

    @BindView(R.id.listOfFilm)
    internal lateinit var recyclerView: RecyclerView

    internal var view: View? = null
    private lateinit var presenter: FilmListPresenter
    private var listFilm: MutableList<Film>? = null
    private var recyclerAdapter: RecyclerAdapter? = null
    private var positionOfList: Parcelable? = null
    private var sharedPreferencesUtil: SharedPreferencesUtil? = null

    companion object {
        const val OVERVIEW_FILM = "overview"
        const val DELETE_MESSAGE = "Delete this item ?"
        const val PARCELS_LIST_FILM_KEY = "FilmList"
        const val PARCELS_POS_LIST_FILM_KEY = "positionOfList"
        const val SORT_FRAGMENT_TAG = "SortDialogFragment"
        const val TOAST_ERROR = "Error"
        const val SORT_BUNDLE_KEY = "Sort_key"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_list_film, container, false)
            view?.let { ButterKnife.bind(this, it) }
            getAppComponent().inject(this)
            sharedPreferencesUtil = SharedPreferencesUtil(context?.applicationContext)
            initFilmListPresenter()
            savedInstanceState.checkSavedState()
        }
        return view
    }

    fun onMenuSortDialogClicked() {
        presenter.onSortDialogClicked()
    }

    override fun showSortDialog(sortType:String) {
        val sortDialogFragment = SortDialogFragment()
        val bundle = Bundle()
        bundle.putString(SORT_BUNDLE_KEY, sortType)
        sortDialogFragment.arguments = bundle
        fragmentManager?.let { sortDialogFragment.show(it, SORT_FRAGMENT_TAG) }
        sortDialogFragment.setSelectedListener(this)
    }

    private fun Bundle?.checkSavedState() = if (isStateSaved) {
        listFilm = Parcels.unwrap(this?.getParcelable(PARCELS_LIST_FILM_KEY))
        positionOfList = this?.getParcelable(PARCELS_POS_LIST_FILM_KEY)
    } else {
        listFilm = ArrayList()
    }

    fun initFilmListPresenter() {
        presenter = FilmListPresenter(filmViewModal, FilmListInteractor(sharedPreferencesUtil), this)
        presenter.onViewCreated()
    }

    override fun progressBarVisible() {
        progressBar.visibility = View.VISIBLE
    }

    override fun addDataResponse(responceList: List<Film>) {
        listFilm?.addAll(responceList)
        listFilm?.let { presenter.requestDataReceived(it) }
    }

    override fun initAdapter() {
        recyclerAdapter = RecyclerAdapter(listFilm,
                { film -> presenter.onFilmClicked(film as Film) },
                { film, position ->
                    presenter.onFilmLongClicked(film as Film, position)
                    false
                })
        val layoutManager = LinearLayoutManager(activity)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = recyclerAdapter
        recyclerView.layoutManager?.onRestoreInstanceState(positionOfList)
    }

    override fun progressBarGone() {
        progressBar.visibility = View.GONE
    }

    override fun onSaveInstanceState(savedInstanceState: Bundle) {
        savedInstanceState.putParcelable(PARCELS_LIST_FILM_KEY, Parcels.wrap<List<Film>>(listFilm))
        savedInstanceState.putParcelable(PARCELS_POS_LIST_FILM_KEY, recyclerView.layoutManager?.onSaveInstanceState())
        super.onSaveInstanceState(savedInstanceState)
    }

    override fun overviewFilm(film: Film) {
        val detailFragment = DetailFragment()
        val bundle = Bundle()
        bundle.putString(OVERVIEW_FILM, film.overview)
        detailFragment.arguments = bundle
        fragmentManager
                ?.beginTransaction()
                ?.replace(R.id.containerFragment, detailFragment)
                ?.addToBackStack(null)
                ?.commit()
    }

    override fun deleteItemDialog(film: Film, position: Int) {
        activity?.let {
            AlertDialog.Builder(it)
                    .setTitle(film.title)
                    .setMessage(DELETE_MESSAGE)
                    .setPositiveButton(android.R.string.yes) { dialog, which ->
                        listFilm?.remove(film)
                        recyclerAdapter?.notifyItemRemoved(position)
                        listFilm?.size?.let { recyclerAdapter?.notifyItemRangeChanged(position, it) }
                    }
                    .setNegativeButton(android.R.string.no) { dialog, which -> }
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show()
        }
    }

    override fun showToast(text: String) {
        activity?.let { Toasty.error(it, TOAST_ERROR + text, Toast.LENGTH_LONG).show() }
    }

    override fun onDateSortClicked() {
        presenter.onSortByDateButtonClicked()
    }

    override fun onRatingSortClicked() {
        presenter.onSortByRateButtonClicked()
    }

    override fun passListFilm() {
        listFilm?.let { presenter.listFilmReceived(it) }
    }

    override fun notifyAdapterOfChanges() {
        recyclerAdapter?.notifyDataSetChanged()
    }

    override fun addSortData(sortedListFilm: List<Film>) {
        listFilm?.clear()
        listFilm?.addAll(sortedListFilm)
    }
}
